# 7langs-7weeks

This repo contains code exercises from the book Seven Languages in Seven Weeks by Bruce A. Tate. along with other simple practice solutions to better understand the language concepts.

### Resources

- [Seven Languages in Seven Weeks](https://www.amazon.com/Seven-Languages-Weeks-Programming-Programmers/dp/193435659X/)
