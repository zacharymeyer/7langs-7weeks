# frozen_string_literal: true

# The Caesar Cipher is a simple substitution cipher where each letter is
# replaced by the letter that is a fixed number of positions down the
# alphabet. For example, with a shift of 3, A would be replaced by D, B
# would become E, and so on.
class CaesarCipher
  def initialize(shift)
    @shift = shift
  end

  def shift(char, base, range)
    if range.positive?
      code = char.ord + @shift
      code -= range if code > base + range - 1
    else
      code = char.ord - @shift
      code += range.abs if code < base
    end
    code.chr
  end

  def encode(str)
    str.each_char.map do |char|
      case char
      when /[a-z]/
        shift char, 97, 26
      when /[A-Z]/
        shift char, 65, 26
      else
        char
      end
    end.join
  end

  def decode(str)
    str.each_char.map do |char|
      case char
      when /[a-z]/
        shift char, 97, -26
      when /[A-Z]/
        shift char, 65, -26
      else
        char
      end
    end.join
  end
end
