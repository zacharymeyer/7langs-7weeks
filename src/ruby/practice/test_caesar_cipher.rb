# frozen_string_literal: true

require 'test/unit'
require_relative 'caesar_cipher'

# Test class for CaesarCipher
class TestCaesarCipher < Test::Unit::TestCase
  def test_encode
    caesar = CaesarCipher.new(5)
    encoding = caesar.encode('What a string!')
    assert_equal 'Bmfy f xywnsl!', encoding

    caesar = CaesarCipher.new(13)
    encoding = caesar.encode('Ruby is fun!')
    assert_equal 'Ehol vf sha!', encoding
  end

  def test_decode
    caesar = CaesarCipher.new(5)
    decoding = caesar.decode('Bmfy f xywnsl!')
    assert_equal 'What a string!', decoding

    caesar = CaesarCipher.new(13)
    decoding = caesar.decode('Ehol vf sha!')
    assert_equal 'Ruby is fun!', decoding
  end
end
