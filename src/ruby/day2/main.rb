# frozen_string_literal: true

puts '---------------------'
puts

# 1)
# Print the contents of an array of sixteen numbers, four numbers at a time, using just each.
# Now, do the same with each_slice in Enumerable.
nums = (1..16).to_a
nums.each do |i|
  if (i % 4).zero?
    puts i.to_s
  else
    print "#{i}, "
  end
end

nums.each_slice(4) { |i| puts i.to_s }

puts
puts '---------------------'
puts

# 2)
# The Tree class was interesting, but it did not allow you to specify a new tree with a clean user interface.
# Let the initializer accept a nested structure of hashes. You should be able to specify a tree like this:
# {'grandpa' => { 'dad' => { 'child 1' => {}, 'child 2' => {} }, 'uncle' => { 'child 3' => {}, 'child 4' => {} } } }.
class Tree
  attr_accessor :children, :node_name

  def initialize(tree)
    @node_name = tree.keys[0]
    @children = []
    tree[@node_name].each { |k, v| @children << Tree.new(k => v) }
  end

  def visit_all(&block)
    visit(&block)
    children.each { |c| c.visit_all(&block) }
  end

  def visit(&block)
    block.call self
  end
end

# ruby_tree = Tree.new( "Ruby", [Tree.new("Reia"), Tree.new("MacRuby")] )
ruby_tree = Tree.new({ 'grandpa' => { 'dad' => { 'child 1' => {}, 'child 2' => {} },
                                      'uncle' => { 'child 3' => {}, 'child 4' => {} } } })

puts 'Visiting a node'
ruby_tree.visit { |node| puts node.node_name }
puts

puts 'Visiting entire tree'
ruby_tree.visit_all { |node| puts node.node_name }

puts
puts '---------------------'
puts

# 3)
# Write a simple grep that will print the lines of a file having any occurrences of a phrase anywhere in that line.
# You will need to do a simple regular expression match and read lines from a file. (This is surprisingly simple in Ruby.) If you want, include line numbers.
def grep(file, phrase)
  File.foreach(file) { |line, i| puts "#{i}: #{line}" if line.match(phrase) }
end

dirname = __dir__
filename = File.join dirname, 'main.rb'

grep filename, 'nums'

puts
puts '---------------------'
puts
