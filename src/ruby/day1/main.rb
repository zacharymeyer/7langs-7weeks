# frozen_string_literal: true

# 1)
# Print the string "Hello, world."
puts 'Hello, world.'

# 2)
# For the string "Hello, Ruby." find the index of the word "Ruby."
puts 'Hello, Ruby.'.index('Ruby.')

# 3)
# Print your name ten times.
n = 0
until n > 10
  puts 'Zachary Meyer'
  n += 1
end

# 4)
# Print the string "This is sentence number 1," where the number 1 changes from 1 to 10
x = 1..10
x.each do |i|
  puts "This is sentence number #{i}"
end

# Bonus
# If you're feeling the need for a little more, write a program that picks a random number.
# Let a player guess the number, telling the player whether the guess is too low or too high.
number = rand(1..10)

puts 'Guess a number between 1 and 10'
guess = gets
until guess.to_i == number
  if guess.to_i > number
    puts 'Too high.'
  else
    puts 'Too low.'
  end
  guess = gets
end

puts "Correct, the number was #{number}"
