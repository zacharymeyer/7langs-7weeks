# frozen_string_literal: true

# Modify the CSV application to support an each method to return a CsvRow object.
# Use the method_missing on that CsvRow to return the value for the column for a given heading.
# For example, for the file:
# one, two
# lions, tigers
# allow an API that works like this:
# csv = RubyCsv.new
# csv.each {|row| puts row.one}
# This should print "lions".
module ActsAsCsv
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end

  module InstanceMethods
    def read
      @csv_contents = []
      filename = "#{self.class.to_s.downcase}.txt"
      file = File.new(filename)
      @headers = file.gets.chomp.split(', ')

      file.each do |row|
        @csv_contents << row.chomp.split(', ')
      end
    end

    attr_accessor :headers, :csv_contents

    def initialize
      read
    end

    def each(&block)
      csv_contents.each do |row|
        block.call CsvRow.new(headers, row)
      end
    end

    class CsvRow
      def initialize(headers, row)
        @headers = headers
        @row = row
      end

      attr_accessor :headers, :row

      def method_missing name, *_args
        index = headers.index(name.to_s)
        row[index]
      end
    end
  end
end

# no inheritance! You can mix it in
class RubyCsv
  include ActsAsCsv
  acts_as_csv
end

m = RubyCsv.new
m.each { |row| puts row.one }
# puts m.headers.inspect
# puts m.csv_contents.inspect
