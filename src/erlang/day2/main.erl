-module(main).
-export([get_value/2]).

get_value(List, Key) ->
    case lists:keyfind(Key, 1, List) of
        {Key, Value} -> Value;
        false -> not_found
    end.
