-module(algos).
-export([prime_numbers/1]).

prime_numbers(N) ->
    % Sieve of Eratosthenes algorithm for finding prime numbers
    % Start with a list of all numbers from 2 to N
    sieve(lists:seq(2, N), []).

sieve(RemainingNumbers, Acc) ->
    % If there are no more numbers, return the list of primes
    case RemainingNumbers of
        % This is the base case
        [] ->
            lists:reverse(Acc);
        _ ->
            % Otherwise, take the next number and filter out all multiples
            [NextPrime | Rest] = RemainingNumbers,
            FilteredNumbers = filter_multiples(NextPrime, Rest),
            % Recurse with the remaining numbers and the next prime
            sieve(FilteredNumbers, [NextPrime | Acc])
    end.

filter_multiples(Prime, Numbers) ->
    lists:filter(fun(N) -> N rem Prime =/= 0 end, Numbers).
