-module(yet_again).
-export([another_factorial/1]).
-export([another_fibonacci/1]).

another_factorial(0) -> 1;
another_factorial(N) -> N * another_factorial(N - 1).

another_fibonacci(0) -> 0;
another_fibonacci(1) -> 1;
another_fibonacci(N) -> another_fibonacci(N - 1) + another_fibonacci(N - 2).
