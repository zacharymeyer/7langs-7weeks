-module(main).
-export([word_count/1]).
-export([print_numbers_from/1]).
-export([err_or_success/1]).

word_count("") ->
    0;
word_count(String) ->
    Rest = string:split(String, " "),
    case Rest of
        [_] ->
            1;
        [_, _ | _] ->
            1 + word_count(lists:flatten(Rest))
    end.

print_numbers_from(N) when N < 11 ->
    io:format("~p ", [N]),
    print_numbers_from(N + 1);
print_numbers_from(_) ->
    io:format("~n").

err_or_success({error, Message}) ->
    io:format("error: ~p~n", [Message]);
err_or_success(success) ->
    io:format("success~n").
